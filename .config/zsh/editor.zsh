### Vim Defaults and Shortcuts
export EDITOR='nvim'

alias v='nvim'
alias vi='nvim'
alias vim='nvim'

# Adds a dumb hack to remap capslock to escape
# because capslock is dumb but ubuntu doesnt seem
# to have a good way to persist this out of the box
# and I am lazy
# and I only care while I am in the terminal
# and cause I want to
if [ -f ~/.Xmodmap ]; then
    # Quitely fail, end of the day if it doesnt work
    # it doesnt matter
    xmodmap ~/.Xmodmap 2> /dev/null || true
fi

