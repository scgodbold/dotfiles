local M = {}

local function bootstrap_pckr()
	local pckr_path = vim.fn.stdpath("data") .. "/pckr/pckr.nvim"

	if not (vim.uv or vim.loop).fs_stat(pckr_path) then
		vim.fn.system({
			"git",
			"clone",
			"--filter=blob:none",
			"https://github.com/lewis6991/pckr.nvim",
			pckr_path,
		})
	end

	vim.opt.rtp:prepend(pckr_path)
end

M.load = function()
	bootstrap_pckr()
	require("pckr").add({
		-- Plenary Lib
		"nvim-lua/plenary.nvim",
		-- File system Navigation
		{
			"stevearc/oil.nvim",
			config = function()
				require("plugin.oil")
			end,
		},
		-- Tmux integration
		{
			"numToStr/Navigator.nvim",
			config = function()
				require("plugin.navigator")
			end,
		},
		-- LSP Support
		{
			"neovim/nvim-lspconfig",
		},
		{
			"williamboman/mason.nvim",
			config = function()
				require("plugin.mason")
			end,
		},
		{
			"williamboman/mason-lspconfig.nvim",
			requires = { "williamboman/mason.nvim", "neovim/nvim-lspconfig", "hrsh7th/cmp-nvim-lsp" },
			config = function()
				require("plugin.masonlspconfig")
			end,
		},
		-- Lint Support
		{
			"mfussenegger/nvim-lint",
		},
		{
			"rshkarin/mason-nvim-lint",
			requires = { "williamboman/mason.nvim", "mfussenegger/nvim-lint" },
			config = function()
				require("plugin.masonlint")
			end,
		},
		-- Formatter
		{
			"mhartington/formatter.nvim",
			config = function()
				require("plugin.formatter")
			end,
		},
		{
			-- A listed definition of installed tools existed for LSPs & Linters but not formatters
			-- this seems to close the gap for now
			"WhoIsSethDaniel/mason-tool-installer.nvim",
			requires = { "williamboman/mason.nvim" },
			config = function()
				require("plugin.masontoolinstaller")
			end,
		},
		-- Snippets
		{
			"L3MON4D3/LuaSnip",
		},
		-- Completion Support
		{
			"hrsh7th/cmp-nvim-lsp",
		},
		{
			"saadparwaiz1/cmp_luasnip",
		},
		{
			"hrsh7th/nvim-cmp",
			requires = { "L3MON4D3/LuaSnip", "saadparwaiz1/cmp_luasnip", "hrsh7th/cmp-nvim-lsp" },
			config = function()
				require("plugin.cmp")
			end,
		},
		-- Auto Pairs
		{
			"windwp/nvim-autopairs",
			config = function()
				require("plugin.autopairs")
			end,
		},
		-- Treesitter Support
		{
			"nvim-treesitter/nvim-treesitter",
			config = function()
				require("plugin.treesitter")
			end,
		},
		-- Git Support
		{
			"f-person/git-blame.nvim",
		},
		-- Colorscheme
		{
			"folke/tokyonight.nvim",
			config = function()
				require("plugin.colorscheme")
			end,
		},
		-- Ansible FT Support
		{
			"mfussenegger/nvim-ansible",
		},
	})
end

return M
