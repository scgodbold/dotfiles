local M = {}

M.load = function()
	local opt = vim.opt
	local g = vim.g
	local cache_path = os.getenv("HOME") .. "/.config/nvim/cache/"

	local global_config = {
		-- disable builtin plugins
		netrw = 1,
		netrwPlugin = 1,
		netrwSettings = 1,
		netrwFileHandlers = 1,
		gzip = 1,
		zip = 1,
		zipPlugin = 1,
		tar = 1,
		tarPlugin = 1,
		getscript = 1,
		getscriptPlugin = 1,
		vimball = 1,
		vimballPlugin = 1,
		html_plugin = 1,
		logipat = 1,
		rrhelper = 1,
		spellfile_plugin = 1,
	}

	local default_options = {
		-- search
		ignorecase = true,
		incsearch = true,
		smartcase = true,
		hlsearch = true,
		grepprg = "rg --vimgrep",
		-- tabs
		expandtab = true,
		smartindent = true,
		smarttab = true,
		shiftwidth = 4,
		tabstop = 4,
		softtabstop = 4,
		-- ui
		wrap = false,
		number = true,
		scrolloff = 8,
		sidescrolloff = 8,
		cursorline = true,
		showcmd = true,
		showmatch = true,
		splitright = true,
		splitbelow = true,
		-- backup & undo
		backup = false,
		undodir = cache_path .. "undo",
		directory = cache_path .. "swap",
		undofile = true,
		undolevels = 500,
		undoreload = 500,
		-- misc
		updatetime = 300,
		completeopt = "menuone,noselect",
	}

	for k, v in pairs(global_config) do
		g[k] = v
	end

	for k, v in pairs(default_options) do
		opt[k] = v
	end
end

return M
