local ok, ts_config = pcall(require, "nvim-treesitter.configs")
if not ok then
    return
end

ts_config.setup({
    ensure_installed = {
        "bash",
        "css",
        "go",
        "html",
        "hcl",
        "javascript",
        "json",
        "lua",
        "make",
        "markdown",
        "python",
        "rust",
        "scss",
        "sql",
        "terraform",
        "toml",
        "typescript",
        "vim",
        "yaml",
    },
    highlight = {
        enable = true,
        use_languagetree = true
    },
    indent = {
        enable = true,
    },
    context_comment_string = {
        enable = true,
    },
})
