local M = {}

M.load = function()
	vim.g.mapleader = " "

	-- Oil
	vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directgory" })

	-- Map splits
	vim.keymap.set("n", "<leader>\\", ":vsplit<cr>", { desc = "create new vertical split" })
	vim.keymap.set("n", "<leader>-", ":split<cr>", { desc = "create new horizontal split" })

	-- Tmux
	vim.keymap.set({ "n", "t" }, "<C-h>", "<CMD>NavigatorLeft<CR>", { desc = "Switch tmux/nvim pane left" })
	vim.keymap.set({ "n", "t" }, "<C-j>", "<CMD>NavigatorDown<CR>", { desc = "Switch tmux/nvim pane down" })
	vim.keymap.set({ "n", "t" }, "<C-k>", "<CMD>NavigatorUp<CR>", { desc = "Switch tmux/nvim pane up" })
	vim.keymap.set({ "n", "t" }, "<C-l>", "<CMD>NavigatorRight<CR>", { desc = "Switch tmux/nvim pane right" })

	-- Flip ; and :
	vim.keymap.set("n", ";", ":", { desc = "Flip ; to behave like :" })

	-- Remove command window binding
	vim.keymap.set(
		"n",
		"q:",
		":q<cr>",
		{ desc = "Alter command window binding to instead be quit, this is typically what I want" }
	)

	-- Number toggles
	vim.keymap.set("n", "<leader>n", ":setlocal number! number?<cr>")
	vim.keymap.set("n", "<leader>N", ":setlocal relativenumber! relativenumber?<cr>")

	-- Clear search highlights
	vim.keymap.set("n", "<leader>c", ":nohlsearch<CR>")

	-- Toggle paste mode
	vim.keymap.set("n", "<leader>p", ":setlocal paste! paste?<cr>")
end

return M
