local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

require("mason-lspconfig").setup({
	ensure_installed = {
		"ansiblels",
		"bashls",
		"dockerls",
		"gopls",
		"helm_ls",
		"jsonls",
		"lua_ls",
		"marksman",
		"ruff",
		-- "r_language_server",
		"tflint",
		"terraformls",
		"vimls",
		"yamlls",
	},
	handlers = {
		function(server_name) -- default handler (optional)
			require("lspconfig")[server_name].setup({
				capabilities = capabilities,
			})
		end,
	},
})
