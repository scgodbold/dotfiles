require("formatter").setup({
	logging = true,
	log_level = vim.log.levels.WARN,
	filetype = {
		lua = {
			require("formatter.filetypes.lua").stylua,
		},
		r = {
			require("formatter.filetypes.r").styler,
		},
		go = {
			require("formatter.filetypes.go").gofmt,
		},
		python = {
			require("formatter.filetypes.python").ruff,
		},
		json = {
			require("formatter.filetypes.json").jq,
		},
		md = {
			require("formatter.filetypes.markdown").mdformat,
		},
		terraform = {
			require("formatter.filetypes.terraform").terraformfmt,
		},
		-- yaml = {
		-- 	require("formatter.filetypes.yaml").yamlfmt,
		-- },
		zsh = {
			require("formatter.filetypes.zsh").beautysh,
		},
		["*"] = {
			require("formatter.filetypes.any").remove_trailing_whitespace,
		},
	},
})

local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

augroup("__formatter__", { clear = true })
autocmd("BufWritePost", {
	group = "__formatter__",
	command = ":FormatWrite",
})
