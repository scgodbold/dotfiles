require("mason-tool-installer").setup({
	ensure_installed = {
		"jq",
		"mdformat",
		"stylua",
		"yamlfmt",
	},
})
