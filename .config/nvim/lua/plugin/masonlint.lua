require("mason-nvim-lint").setup({
	ensure_installed = {
		"ansible-lint",
		"luacheck",
		"ruff",
	},
	ignore_install = {
		"janet",
		"clj-kondo",
		"inko",
		"ruby",
	},
})
