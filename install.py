#!/usr/bin/env python
import os
import shutil
import subprocess
from optparse import OptionParser


# parse all of the things!
parser = OptionParser()
parser.add_option('-u', '--uninstall', dest='uninstall', default=True, action='store_false',
    help='uninstall dotfiles')
parser.add_option('-q', '--quite', dest='verbose', default=True, action='store_false',
    help='Mutes output messages from the installer')
parser.add_option('-p', '--preserveFS', dest='perserveFS', default=True, action='store_false',
    help='When uninstalling, call this to assure preservation of the folders created by this installer')
parser.add_option('-v', '--vimPlugins', dest='vim', default=True, action='store_false',
    help='Call this to only reinstall vim plugins')


# Set installation targets and python hooks
TARGETS = {
    'src/vimrc': '',
    'src/vim': '',
    'src/tmux.conf': '',
    'src/zshrc': '',
}
PYHOOKS = {
    'src/postmkvirtualenv': '.virtualenvs',
    'src/postactive': '.virtualenvs',
}


# Internal use functions
def verbosePrint(options, string):
    """ Prints only if the user has not opted out of messages """
    if options.verbose:
        print string


def bash(command):
    # allows for a bash command to be passed in as a string
    # and its stdout is returned, this isn't safe but fuck it
    output = subprocess.Popen([command], shell=True,
        stdout=subprocess.PIPE).communicate()[0]
    return output


def rmPath(path):
        """ Removes path if it exists """
        if os.path.exists(path):
                if os.path.isdir(path):
                        shutil.rmtree(path)
                else:
                        os.remove(path)

# Installation & deletion functions


def mkFS(options):
        """ Creates the file structure for the dotfiles """
        try:
                verbosePrint(options, 'creating directory: %s/repos/py' % options.home)
                os.makedirs('%s/repos/py' % options.home)
        except:
                verbosePrint(options, 'py directory already exists')
        try:
                verbosePrint(options, 'creating directory: %s/.virtualenv' % options.home)
                os.makedirs('%s/.virtualenvs' % options.home)
        except:
                verbosePrint(options, 'virtualenvs directory already exists')


def rmFS(options):
        if options.perserveFS:
                verbosePrint(options, 'removing directory: %s/repos/py' % options.home)
                rmPath('%s/repoys/py' % options.home)
                verbosePrint(options, 'removing directory: %s/.virtualenv' % options.home)
                rmPath('%s/.virtualenvs' % options.home)


def initVundle(options):
        """ There is no uninstall for this as it is all contained in the dotfile folder """
        vimdir = '%s/src/vim' % options.cwd
        if not os.path.exists(vimdir + '/bundle/Vundle.vim'):
                bash('git clone https://github.com/gmarik/Vundle.vim.git ' + vimdir + '/bundle/Vundle.vim')


def installVundlePlugins(options):
        """Installs vundle plugins for vim no uninstall needed its all selfcontained"""
        vimdir = '%s/src/vim' % options.cwd
        if os.path.exists(vimdir + '/bundle/Vundle.vim'):
                bash('vim +PluginInstall +qall')
                return True
        return False


def installZsh(options):
        """Installs ohmyzsh from web"""
        if not os.path.exists(options.home + '/.oh-my-zsh'):
                return bash('wget --no-check-certificate http://install.ohmyz.sh -O - | sh')


def removeZsh(options):
        """ Removes oh my zsh """
        if os.path.exists('%s/.oh-my-zsh' % options.home):
                return bash('rm -rf %s/.oh-my-zsh' % options.home)


def install(options):
        """Installs conf files from source to home directory"""
        verbosePrint(options, 'Starting to install conf files')
        for item in TARGETS:
                try:
                        name = '.' + item.split('/')[1]
                        if TARGETS[item] is not '':
                                dest = options.home + '/' + TARGETS[item] + '/' + name
                        else:
                                dest = options.home + '/' + name
                        verbosePrint(options, 'placing ' + name + ' at location: ' + dest)
                        rmPath(dest)
                        os.symlink(options.cwd + '/' + item, dest)
                except:
                        verbosePrint(options, 'failed to place: %s' % item.split('/')[1])


def uninstall(options):
        """ Removes the installed dotfiles """
        verbosePrint(options, 'Starting to remove conf files')
        for item in TARGETS:
                try:
                        name = '.' + item.split('/')[1]
                        if TARGETS[item] is not '':
                                dest = options.home + '/' + TARGETS[item] + '/' + name
                        else:
                                dest = options.home + '/' + name
                        verbosePrint(options, 'removing ' + name + ' at location: ' + dest)
                        rmPath(dest)
                except:
                        verbosePrint(options, 'failed to remove: %s' % item.split('/')[1])


def installPyhooks(options):
        """Used to place virtualenv hooks into place, different method then rest of conf files"""
        for item in PYHOOKS:
                try:
                        name = item.split('/')[1]
                        if PYHOOKS[item] is not '':
                                dest = options.home + '/' + PYHOOKS[item] + '/' + name
                        else:
                                dest = options.home + '/' + name
                        verbosePrint(options, 'placing ' + name + ' at location: ' + dest)
                        rmPath(dest)
                        os.symlink(options.cwd + '/' + item, dest)
                except:
                        verbosePrint(options, 'failed to place: %s' % item.split('/')[1])


def removePyhooks(options):
        """Used to place virtualenv hooks into place, different method then rest of conf files"""
        for item in PYHOOKS:
                try:
                        name = item.split('/')[1]
                        if PYHOOKS[item] is not '':
                                dest = options.home + '/' + PYHOOKS[item] + '/' + name
                        else:
                                dest = options.home + '/' + name
                        verbosePrint(options, 'removing ' + name + ' at location: ' + dest)
                        rmPath(dest)
                except:
                        verbosePrint(options, 'failed to remove: %s' % item.split('/')[1])


# The Main Program
def main(options):
        print options
        if not options.vim:
            installVundlePlugins(options)
        elif not options.uninstall:
                # Uninstall the dotfiles
                removePyhooks(options)
                uninstall(options)
                removeZsh(options)
                rmFS(options)
        else:
                # install the dotfiles
                mkFS(options)
                installZsh(options)
                initVundle(options)
                install(options)
                installPyhooks(options)
                installVundlePlugins(options)
                verbosePrint(options, 'Installation successful!')


if __name__ == '__main__':
        (options, args) = parser.parse_args()
        options.home = bash('cd ~/ && pwd')[:-1]
        verbosePrint(options, 'Home set so setting it to %s' % options.home)
        options.cwd = bash('pwd')[:-1]
        verbosePrint(options, 'Our current working directory is: %s' % options.cwd)
        main(options)
